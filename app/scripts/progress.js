const progress = () => {
  const docHeight = $(document).height();
  const windowHeight = $(window).height();
  $(window).scroll(function() {
    const scrollPercent = $(window).scrollTop() / (docHeight - windowHeight) * 100;

    $('.menu__progress').css('height', scrollPercent + '%');
  });
}
export default progress;
