const ajax = () => {
  $('.block').on('click', function() {
    $(this).children().css('visibility', 'hidden');
    $(this).find('.img').css('visibility', 'visible');
    $.ajax({
      type: "get",
      url: "/ajax.html",
      dataType: 'html',
      success: function(data) {
        function func() {
          $('.block').html(data)
        }
        setTimeout(func, 3000);
        window.history.replaceState('object or string', 'Title', '/hemeon');
      }
    });
  });
}
export default ajax;
