const onScrollAnimate = (elem) => {
  $(window).on('scroll', () => {
    const docViewTop = $(window).scrollTop();
    const docViewBottom = docViewTop + $(window).height();
    const elemTop = $(elem).offset().top;
    const elemBottom = elemTop + $(elem).height();

    if ((elemBottom <= docViewBottom) && (elemTop >= docViewTop)) {
      $(elem).addClass('animate')
    }
  });
}
export default onScrollAnimate;
