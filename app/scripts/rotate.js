const rotate = (number) => {
  const document = window.document;
  const handlers = [];
  $(window).on('load', () => {
    const start = $(number).offset().top - 200;
    const elements = $('[data-rotate]');
    for (let i = 0; i < elements.length; i++) {
      elements[i].dataTranslate = elements[i].getAttribute('data-rotate');
      // console.log(elements[i]);
    }
    handlers.push(
      () => {
        const scroll = window.pageYOffset - start;
        for (let i = 0; i < elements.length; i++) {
          elements[i].style.transform = `rotateY(${elements[i].dataTranslate*-scroll}deg)`;
        }
      }
    );
    $(window).on('scroll', handlers[handlers.length - 1]);
  })
}
export default rotate;
