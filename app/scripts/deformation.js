const deformation = (deformation) => {
  if (!deformation) return;
  deformation.parent().on('mousemove',function(e) {
    let x = e.offsetX || e.originalEvent.layerX;
    let y = e.offsetY || e.originalEvent.layerY;
    // let x = e.offsetX==undefined ? e.layerX : e.offsetX;
    // let y = e.offsetY==undefined ? e.layerY : e.offsetY;
    let widthBlock = deformation.parent().width() / 2;
    let heightBlock = deformation.parent().height() / 2;

    deformation.each(function () {
        var valueDeformation = $(this).attr('data-deformation');
        $(this).css({
          transform: `rotate3d(${-(y - heightBlock)}, ${x - widthBlock}, 0,${valueDeformation}deg)`
        });
      });
  });
  deformation.parent().on('mouseout', function(e) {
    deformation.attr('style', '')
  });
  }













 // if (!deformation) return;
//  $('.block').on('mousemove', function(e) {
//    let middleX = $(window).width() / 2;
//    let middleY = $(window).height() / 2;
//    let x = e.pageX;
//    let y = e.pageY;
//    console.log(y);
  //  deformation.css({
  //    transform: `rotate3d(${(y - middleY)}, ${(x - middleX)}, 0,10deg)`
  //  });
//  });
 // $('.block').on('mouseout', function(e) {
 //   $('.img').attr('style', '')
 // });
//
// }
export default deformation;
