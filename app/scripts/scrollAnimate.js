const scrollAnimate = (node) => {
  $(window).on('scroll', () => {
    const elements = $('[data-translate]');
    elements.each(function() {
      const start = $(this).closest('.wrap-animate').offset().top;
      var valueDeformation = $(this).attr('data-translate');
      const scroll = $(window).scrollTop() - start;
      $(this).css({
        transform: `translate3d(0,${valueDeformation*-scroll}px,0)`
      })
    });
  });

}
export default scrollAnimate;





// $(window).on('load', () => {
// +    const start = $(node).offset().top - 200;
// +    const elements = $('[data-translate]');
// +    for (let i = 0; i < elements.length; i++) {
// +      elements[i].dataTranslate = elements[i].getAttribute('data-translate');
// +    }
// +    handlers.push(
// +      () => {
// +        const scroll = window.pageYOffset - start;
// +        for (let i = 0; i < elements.length; i++) {
// +          elements[i].style.transform = `translate3d(0,${elements[i].dataTranslate*-scroll}px,0)`;
// +        }
// +      }
// +    );
