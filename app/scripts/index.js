import scrollAnimate from "./scrollAnimate";
import ajax from "./ajax";
import rotate from "./rotate";
import deformation from "./deformation";
import onScrollAnimate from "./onScrollAnimate";
import progress from "./progress";
import popup from "./popup";
import heading from "./heading";

$(() => {
  scrollAnimate($('[data-translate]'));
  ajax();
  rotate($('[data-rotate]'));
  deformation($('.img, .anim-text, .box'));
  onScrollAnimate('.text, .title, .block');
  progress();
  popup('.block');
  heading('.text-bottom span');
  const url = window.location.pathname.toString()
});
